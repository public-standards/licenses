# Copyright Statement

The software package contained within this Spec Validator allows users to verify that an API meets the Open Retailing Design Guidelines. The Spec Validator is jointly copyrighted by Conexxus and IFSF.  All rights are expressly reserved.

**IF YOU ACQUIRE THIS SOFTWARE FROM IFSF, THE FOLLOWING STATEMENT ON THE USE OF COPYRIGHTED MATERIAL APPLIES:**

You may download the Spec Validator to a local hard disk for your own business use without modification. Any other redistribution or reproduction of part or all of the contents in any form is prohibited.

You may not, except with our express written permission, distribute to any third party.  Where permission to distribute is granted by IFSF, the material must be acknowledged as IFSF copyright.

You agree to abide by all copyright notices and restrictions attached to the content and not to remove or alter any such notice or restriction.

No part of the Spec Validator may be claimed as the Intellectual property of any organisation other than IFSF Ltd. and Conexxus, Inc., and you specifically agree not to claim patent rights or other IPR protection that relates to:

1. the content of the spec validator; or
2. any design or part thereof that embodies the content of the spec validator whether in whole or part.

For further copies and amendments to this document please contact: IFSF Technical Services via the IFSF Web Site ([www.ifsf.org](http://www.ifsf.org)).

**IF YOU ACQUIRE THIS DOCUMENT FROM CONEXXUS, THE FOLLOWING STATEMENT ON THE USE OF COPYRIGHTED MATERIAL APPLIES:**

Conexxus members may use the Spec Validator ONLY for purposes consistent with the adoption of the Conexxus Standard (and/or the related documentation), as detailed in the applicable Implementation Guide. No other uses will be approved by Conexxus.

A Member shall not modify, adapt, merge, transform, copy, or create derivative works of the Spec Validator, including the documentation suite and the application programing interface (“API”).  Conexxus recognizes that the API may include multiple Definition Files, and accordingly recognizes and agrees that the Member may implement one, some, or all Definition Files within the API, unless otherwise specified in the Implementation Guide, provided that each Definition File implemented is implemented in full.  Here implementing a Definition File in full means that all functionality defined by the Conexxus Standard for the Definition File is implemented.  Regardless of whether the Member implements one, some, or all Definition Files, the Member agrees to abide by all requirements under this Copyright Statement for each of the Definition Files implemented.

Note that some functionality within a Definition File is specified for predefined error or non-implementation codes to be returned.  For functionality where such predefined codes are specified, returning such a predefined code constitutes an implementation.  However, in such cases, a Member may not return codes or values different from the predefined codes, nor may the Member simply not implement the functionality, as this would create a Definition File that is not fully implemented as required under this Copyright Statement.

A Member hereby waives and agrees not to assert or take advantage of any defense based on copyright fair use. A Member, as well as any and all of the Member’s development partners who are responsible for implementing the Spec Validator for the Member or may have access to the Conexxus Standard, must be made aware of, and agree to comply with, all requirements under this Copyright Statement prior to accessing any documentation or API. No derivative works of the Spec Validator shall be permitted or approved by Conexxus.

A Conexxus Member shall require its development partners to download the Spec Validator directly from the Conexxus website.  A Conexxus Member may not furnish the Spec Validator in any form to non-members of Conexxus or to Conexxus Members who do not possess document rights or who are not direct contractors of the Member, including to any direct contractor of the Member who does not agree in writing to comply with the terms of this Copyright Statement.  A Member may demonstrate its Conexxus membership at a level that includes document rights by presenting an unexpired digitally signed Conexxus membership certificate. In addition, the Spec Validator, in whole or in part, may not be submitted as input to generative AI systems without the express prior written permission of Conexxus.  In no case will Conexxus grant permission for use with any generative AI system  without a commitment from the proposed user to follow clear terms and conditions protecting submitted intellectual property.

The Spec Validator may not be modified in any way, including removal of the copyright notice or references to Conexxus. If a Member desires to make draft changes to the API code for trial use, those changes must first be submitted to Conexxus for consideration to be included in the existing API before any formal use of those changes is permitted.

The limited permissions related to the Spec Validator granted above are perpetual and will not be revoked by Conexxus, Inc. or its successors or assigns, except in the circumstance where an entity, who is no longer a member in good standing but who rightfully obtained Conexxus Standards as a former member, is acquired by a non-member entity. In such circumstances, Conexxus may revoke the grant of limited permissions or require the acquiring entity to establish rightful access to the Spec Validator through membership.

Disclaimers

**IF YOU ACQUIRE THIS DOCUMENT FROM CONEXXUS OR IFSF, THE FOLLOWING DISCALIMER STATEMENT APPLIES:**

Conexxus and IFSF make no warranty, express or implied, about, nor do they assume any legal liability or responsibility for, the accuracy, completeness, or usefulness of this Spec Validator, even if such liability was disclosed to Conexxus, IFSF, or was foreseeable.  This software is expressly provided “as is.” Although Conexxus and IFSF use commercially reasonable best efforts to ensure this work product is free of any encumbrances from third-party intellectual property rights (IPR), it cannot guarantee that such IPR does not exist now or in the future.  Conexxus and IFSF further notifies each user of the Spec Validator that its individual method of implementation may result in infringement of the IPR of others.  Accordingly, each user is encouraged to seek legal advice from competent counsel to carefully review its implementation of this standard and obtain appropriate licenses where needed.