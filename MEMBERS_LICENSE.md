# Copyright Statement

## Copyright © IFSF and CONEXXUS, INC., All Rights Reserved.

The content (content being images, text or any other medium contained within this document which is eligible of copyright protection) are jointly copyrighted by Conexxus and IFSF. All rights are expressly reserved.

### IF YOU ACQUIRE THIS DOCUMENT FROM IFSF. THE FOLLOWING STATEMENT ON THE USE OF COPYRIGHTED MATERIAL APPLIES:

You may print or download to a local storage fully under your control and responsibility, extracts for your own business use. Any other redistribution or reproduction of part or all of the contents in any form is prohibited.

You may not, except with our express written permission, distribute any of the copyrighted document(s), including the API, to any third party. Where permission to distribute is granted by IFSF, the material must be acknowledged as copyrighted by the IFSF and the document title specified.  Where third-party material has been identified, permission from the respective copyright holder must be sought.

You may not, except with our express written permission, and only under the terms accompanying our permission therein, modify, adapt, merge, transform, copy, or create derivative works of the Standard, including the documentation suite and the application programing interface (“API”).  You hereby waive and agree not to assert or take advantage of any defense based on copyright fair use.

You agree to abide by all copyright notices and restrictions attached to the content and not to remove or alter any such notice or restriction.

Subject to the following paragraph, you may design, develop and offer for sale products which embody the functionality described in this document.

No part of the content of this document may be claimed as the Intellectual property of any organisation other than IFSF Ltd or Conexxus, and you specifically agree not to claim patent rights or other Intellectual Property Rights (IPR) protection that relates to:
1.	the content of this document; or
2.	any design or part thereof that embodies the content of this document, whether in whole or part.
For further copies and amendments to this document please contact: IFSF Technical Services via the IFSF Web Site ([www.ifsf.org](http://www.ifsf.org/)).

### IF YOU ACQUIRE THIS DOCUMENT FROM CONEXXUS, THE FOLLOWING STATEMENT ON THE USE OF COPYRIGHTED MATERIAL APPLIES:

Conexxus members may use this document for purposes consistent with the adoption of the Conexxus Standard (and/or the related documentation), as detailed in the Implementation Guide; however, Conexxus must pre-approve any inconsistent uses in writing.

Except in the limited case set forth explicitly in this Copyright Statement, the Member shall not modify, adapt, merge, transform, copy, or create derivative works of the Conexxus Standard, including the documentation suite and the application programing interface (“API”).  Conexxus recognizes that the API may include multiple Definition Files, and accordingly recognizes and agrees that the Member may implement one, some, or all Definition Files within the API, unless otherwise specified in the Implementation Guide, provided that each Definition File implemented is implemented in full.  Here implementing a Definition File in full means that all functionality defined by the Conexxus Standard for the Definition File is implemented.  Regardless of whether the Member implements one, some, or all Definition Files, the Member agrees to abide by all requirements under this Copyright Statement for each of the Definition Files implemented.

Note that some functionality within a Definition File is specified for predefined error or non-implementation codes to be returned.  For functionality where such predefined codes are specified, returning such a predefined code constitutes an implementation.  However, in such cases, a Member may not return codes or values different from the predefined codes, nor may the Member simply not implement the functionality, as this would create a Definition File that was not fully implemented as required under this Copyright Statement. 

The Member hereby waives and agrees not to assert or take advantage of any defense based on copyright fair use. The Member, as well as any and all of the Member’s development partners who are responsible for implementing the Conexxus Standard for the Member or may have access to the Conexxus Standard, must be made aware of, and agree to comply with, all requirements under this Copyright Statement prior to accessing any documentation or API. 

Conexxus recognizes the limited case where a Member wishes to create a derivative work that comments on, or otherwise explains or assists in its own implementation, including citing or referring to the standard, specification, code, protocol, schema, or guideline, in whole or in part. The Member may do so ONLY for the purpose of explaining or assisting in its implementation of the Conexxus Standard and the Member shall acquire no right to ownership of such derivative work. Furthermore, the Member may share such derivative work ONLY with another Conexxus Member who possesses appropriate document rights or with an entity that is a direct contractor of the Conexxus Member who is responsible for implementing the standard for the Member. In so doing, a Conexxus Member shall require its development partners to download Conexxus documents, API, and schemas directly from the Conexxus website.  A Conexxus Member may not furnish this document in any form, along with any derivative works, to non-members of Conexxus or to Conexxus Members who do not possess document rights or who are not direct contractors of the Member, including to any direct contractor of the Member who does not agree in writing to comply with the terms of this Copyright Statement.  A Member may demonstrate its Conexxus membership at a level that includes document rights by presenting an unexpired digitally signed Conexxus membership certificate. In addition, this document, in whole or in part, may not be submitted as input to generative AI systems without the express prior written permission of Conexxus.  In no case will Conexxus grant permission for use with any generative AI system without a commitment from the proposed user to follow clear terms and conditions protecting submitted intellectual property.

This document may not be modified in any way, including removal of the copyright notice or references to Conexxus. However, a Member has the right to make draft changes to schema or API code for trial use, which must then be submitted to Conexxus for consideration to be included in the existing standard. Translations of this document into languages other than English shall continue to reflect the Conexxus copyright notice.

The limited permissions granted above are perpetual and will not be revoked by Conexxus, Inc. or its successors or assigns, except in the circumstance where an entity, who is no longer a member in good standing but who rightfully obtained Conexxus Standards as a former member, is acquired by a non-member entity. In such circumstances, Conexxus may revoke the grant of limited permissions or require the acquiring entity to establish rightful access to Conexxus Standards through membership.

## Disclaimers

### IF YOU ACQUIRE THIS DOCUMENT FROM CONEXXUS, THE FOLLOWING DISCLAIMER STATEMENT APPLIES:

Conexxus makes no warranty, express or implied, about, nor does it assume any legal liability or responsibility for, the accuracy, completeness, or usefulness of any information, product, or process described in these materials, even if such liability was disclosed to Conexxus or was foreseeable.  Although Conexxus uses commercially reasonable best efforts to ensure this work product is free of any encumbrances from third-party intellectual property rights (IPR), it cannot guarantee that such IPR does not exist now or in the future.  Conexxus further notifies each user of this standard that its individual method of implementation may result in infringement of the IPR of others.  Accordingly, each user is encouraged to seek legal advice from competent counsel to carefully review its implementation of this standard and obtain appropriate licenses where needed.
